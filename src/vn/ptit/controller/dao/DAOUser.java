/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.ptit.controller.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import vn.ptit.model.Player;
import vn.ptit.model.User;

/**
 *
 * @author Cuong Pham
 */
public class DAOUser extends IDAO<User>{

    public DAOUser(Connection conn) {
        this.conn = conn;
        try {
            this.statement = this.conn.createStatement();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
    
    @Override
    public List<User> selectAll() {
        List<User> result = new ArrayList<>();
        try {
            String sql = "SELECT * FROM tbl_user";

            rs = statement.executeQuery(sql);
            while (rs.next()) {
                User user = new User();
                user.setUsername(rs.getString("username"));
                user.setPassword(rs.getString("password"));
                user.setFullname(rs.getString("fullname"));
                user.setEmail(rs.getString("email"));
                user.setImg(rs.getString("img"));
                user.setId(rs.getInt("id"));
                result.add(user);
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }

        return result;

    }

    @Override
    public List<User> selectByName(String name) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int insert(User user) {
        String sql = "INSERT INTO tbl_user (img, username, password, fullname, email) VALUES (?,?,?,?,?)";
        try {
            this.preStatement = this.conn.prepareStatement(sql);
            this.preStatement.setString(1, user.getImg());
            this.preStatement.setString(2, user.getUsername());
            this.preStatement.setString(3, user.getPassword());
            this.preStatement.setString(4, user.getFullname());
            this.preStatement.setString(5, user.getEmail());
            
            int rowCount = this.preStatement.executeUpdate();
            return rowCount;
        } catch (SQLException ex) {
            System.out.println(ex);
            return 0;
        }
    }

    @Override
    public int update(User object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void closeConnection() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public User checkLogin(String username, String password) {
        String sql = "SELECT * FROM tbl_user WHERE username = ? AND password = ?";
        try {
            User user = new User();
            this.preStatement = this.conn.prepareStatement(sql);
            this.preStatement.setString(1, username);
            this.preStatement.setString(2, password);
            
            rs = this.preStatement.executeQuery();
            if(rs.next()){ 
                user.setId(rs.getInt("id"));
                user.setImg(rs.getString("img"));
                user.setUsername(username);
                user.setPassword(password);
                user.setFullname(rs.getString("fullname"));
                user.setEmail(rs.getString("email"));
                return user;
            }
        } catch (SQLException ex) {
            System.out.println(ex);
            return null;
        }
        return null;
    }
    
}
