/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.ptit.controller.sockethandler;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import vn.ptit.controller.sockethandler.ClientHandler;

/**
 *
 * @author Cuong Pham
 */
public class RoomHandler {
    private String id;
    private List<ClientHandler> clients;
    private Date createAt;

    public RoomHandler(String id) {
        this.id = id;
        clients = new ArrayList<>();
        createAt = new Date();
    }
    
    public boolean addClient(ClientHandler c) {
        if (!clients.contains(c)) {
            clients.add(c);
            return true;
        }
        return false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<ClientHandler> getClients() {
        return clients;
    }

    public void setClients(List<ClientHandler> clients) {
        this.clients = clients;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }
    
}
