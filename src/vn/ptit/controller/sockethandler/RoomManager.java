/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.ptit.controller.sockethandler;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Cuong Pham
 */
public class RoomManager {
    private List<RoomHandler> rooms;

    public RoomManager() {
        this.rooms = new ArrayList<>();
    }
    
    public void add(RoomHandler r){
        rooms.add(r);
    }
    
    public void remove(RoomHandler r){
        rooms.remove(r);
    }

    public List<RoomHandler> getRooms() {
        return rooms;
    }

    public void setRooms(List<RoomHandler> rooms) {
        this.rooms = rooms;
    }
    
}
