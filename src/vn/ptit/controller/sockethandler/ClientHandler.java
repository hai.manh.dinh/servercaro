/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.ptit.controller.sockethandler;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.security.SecureRandom;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import vn.ptit.ServerMain;
import vn.ptit.controller.dao.DAOUser;
import vn.ptit.model.Player;
import vn.ptit.model.User;
import vn.ptit.utils.StreamData;
import vn.ptit.utils.RandomString;

/**
 *
 * @author Cuong Pham
 */
public class ClientHandler extends Thread {

    private Socket socket;
    private DataInputStream dataInputStream;
    private DataOutputStream dataOutputStream;
    private Connection conn;
    private DAOUser dAOUser;
    private Player player;

    public ClientHandler(Socket socket, Connection conn) throws IOException {
        this.socket = socket;
        this.dataInputStream = new DataInputStream(socket.getInputStream());
        this.dataOutputStream = new DataOutputStream(socket.getOutputStream());
        this.conn = conn;
        dAOUser = new DAOUser(conn);
    }

    @Override
    public void run() {
        String received;
        while (true) {
            try {
                received = dataInputStream.readUTF();
                StreamData.Type type = StreamData.getTypeFromData(received);
                switch (type) {
                    case SIGNUP:
                        onReceiveSignup(received);
                        break;
                    case LOGIN:
                        onReceiveLogin(received);
                        break;
                    case CONNECT_SERVER:
                        onReceiveConnectServer(received);
                        break;
                    case LIST_ONLINE:
                        onReceiveListOnline(received);
                        break;
                    case CREATE_ROOM:
                        onReceiveCreateRoom(received);
                        break;
                    case LIST_ROOM:
                        onReceiveListRoom(received);
                        break;
                    case JOIN_ROOM:
                        onReceiveJoinRoom(received);
                        break;
                }
            } catch (IOException ex) {
                System.out.println(ex);
            }

        }
    }

    private void onReceiveSignup(String received) {
        // get data from received
        String[] splitted = received.split(";");
        String img = splitted[1];
        String username = splitted[2];
        String password = splitted[3];
        String fullName = splitted[4];
        String email = splitted[5];

        User user = new User(img, username, password, fullName, email);
        int flag = dAOUser.insert(user);
        if (flag > 0) {
            sendData(StreamData.Type.SIGNUP.name() + ";" + "success");
        } else {
            sendData(StreamData.Type.SIGNUP.name() + ";" + "failure");
        }

    }

    private void sendData(String data) {
        try {
            dataOutputStream.writeUTF(data);
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    private void onReceiveLogin(String received) {
        String[] splitted = received.split(";");

        String username = splitted[1];
        String password = splitted[2];

        User user = dAOUser.checkLogin(username, password);
        if (user != null) {
            List<ClientHandler> clientHandlers = ServerMain.clientManager.getClients();
            for (ClientHandler clientHandler : clientHandlers) {
                if (clientHandler.player != null && clientHandler.player.getUser().getUsername().equalsIgnoreCase(username)) {
                    sendData(StreamData.Type.LOGIN.name() + ";" + "failure");
                    return;
                }
            }

            player = new Player(user, "Trực tuyến");
            sendData(StreamData.Type.LOGIN.name() + ";" + "success;" + user.getUsername());
        } else {
            sendData(StreamData.Type.LOGIN.name() + ";" + "failure");
        }

    }

    private void onReceiveConnectServer(String received) {
        sendData(received);
    }

    private void onReceiveListOnline(String received) {
        List<ClientHandler> clients = ServerMain.clientManager.getClients();
        String res = StreamData.Type.LIST_ONLINE.name() + ";success;";
        for (ClientHandler client : clients) {
            if (client.player != null) {
                res += client.player.getUser().getUsername() + ";" + client.player.getStatus() + ";";
            }
        }
        System.out.println(res);
        for (ClientHandler client : clients) {
            if (client.player != null) {
                client.sendData(res);
            }
        }

    }

    private void onReceiveCreateRoom(String received) {
        RandomString randomString = new RandomString(9, new SecureRandom(), RandomString.digits);
        String randomId = randomString.nextString();
        RoomHandler roomHandler = new RoomHandler(randomId);
        this.player.setStatus("Đang tạo trận");
        roomHandler.addClient(this);
        ServerMain.roomManager.add(roomHandler);
        sendData(StreamData.Type.CREATE_ROOM.name() + ";" + "success;" + player.getUser().getUsername() + ";" + player.getUser().getImg() + ";" + this.player.getStatus());
    }

    private void onReceiveListRoom(String received) {
        String res = StreamData.Type.LIST_ROOM.name() + ";success;";
        List<RoomHandler> roomHandlers = ServerMain.roomManager.getRooms();
        for (RoomHandler roomHandler : roomHandlers) {
            res += roomHandler.getId() + ";" + roomHandler.getClients().get(0).getPlayer().getUser().getUsername() + ";" + roomHandler.getClients().size() + "/2;";
        }
        System.out.println(res);
        for (ClientHandler client : ServerMain.clientManager.getClients()) {
            if (client.player != null) {
                client.sendData(res);
            }
        }
    }

    public Player getPlayer() {
        return player;
    }

    private void onReceiveJoinRoom(String received) {
        String[] splitted = received.split(";");
        List<RoomHandler> roomHandlers = ServerMain.roomManager.getRooms();
        RoomHandler room = null;
        for (RoomHandler roomHandler : roomHandlers) {
            if (roomHandler.getId().equalsIgnoreCase(splitted[1])) {
                room = roomHandler;
                break;
            }
        }
        player.setStatus("Trong phòng");
        room.addClient(this);
        String data = StreamData.Type.JOIN_ROOM.name() + ";" + "success;";
        for (ClientHandler clientHandler : room.getClients()) {
            data += clientHandler.player.getUser().getUsername() + ";" + clientHandler.player.getUser().getImg() + ";" + clientHandler.player.getStatus() + ";";
        }
        System.out.println(data);
        sendData(data);
        room.getClients().get(0).sendData(StreamData.Type.CHECK_ROOM.name() + ";" + "success;" + player.getUser().getUsername() + ";" + player.getUser().getImg() + ";" + player.getStatus() + ";");
    }

}
