/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.ptit.controller.sockethandler;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Cuong Pham
 */
public class ClientManager {
    List<ClientHandler> clients;

    public ClientManager() {
        clients = new ArrayList<>();
    }
    
    public void add(ClientHandler c){
        clients.add(c);
    }
    
    public void remove(ClientHandler c){
        clients.remove(c);
    }

    public List<ClientHandler> getClients() {
        return clients;
    }

    public void setClients(List<ClientHandler> clients) {
        this.clients = clients;
    }
    
}
