/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.ptit.model;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Cuong Pham
 */
public class Room {
    private String id;
    private List<Player> players;
    private Player createBy;
    private Date createAt;

    public Room() {
    }

    public Room(String id, List<Player> players, Player createBy, Date createAt) {
        this.id = id;
        this.players = players;
        this.createBy = createBy;
        this.createAt = createAt;
    }

    public Player getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Player createBy) {
        this.createBy = createBy;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public String getId() {
        return id;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }
    
}
