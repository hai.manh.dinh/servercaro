package vn.ptit;


import vn.ptit.controller.dao.ConnectionUtils;
import vn.ptit.controller.sockethandler.ClientHandler;
import vn.ptit.controller.sockethandler.ClientManager;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import vn.ptit.controller.sockethandler.RoomManager;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Cuong Pham
 */
public class ServerMain {

    public static ClientManager clientManager;
    public static ServerSocket serverSocket;
    public static Connection connection;
    public static RoomManager roomManager;

    public ServerMain() throws IOException, SQLException, ClassNotFoundException {
        clientManager = new ClientManager();
        roomManager = new RoomManager();
        connection = ConnectionUtils.getMyConnection();
        int port = 6969;
        serverSocket = new ServerSocket(port);
        System.out.println("Created Server at port " + port);
        while (true) {
            Socket socket = serverSocket.accept();
            System.out.println("New client request received : " + socket);
            ClientHandler client = new ClientHandler(socket, connection);
            clientManager.add(client);
            client.start();
        }
    }

    public static void main(String[] args) {
        try {
            ServerMain serverMain = new ServerMain();
        } catch (IOException ex) {
            System.out.println(ex);
        } catch (SQLException ex) {
            System.out.println(ex);
        } catch (ClassNotFoundException ex) {
            System.out.println(ex);
        }
    }

}
